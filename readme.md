# The timevault

Smart contracts are generally seen as over promising and under delivering. A big reason for this is that there are so little amount of end-user-facing devices actually providing additional functionality based o them.

To help solve this this document attempts to describe the concept of the timevault in normal English that can be understood by people able to build applications and create user experiences fitting to the idea.

The timevault as a concept is one where we create a smart contract and deposit funds into it. We then create 3 roles:

1. the beneficiary  
   The person that takes all the funds at the expiration date of the vault.
2. the owner  
   The owner can always take all the funds, breaking open the vault.
3. the (optional) extender.  
   This role is able to refresh the timeout period to start again.


# Inheritance usecase

Using the above parts we can build an inheritance service. Completely non-custodial. We base this on the well known idea of a "[dead man's switch](https://en.wikipedia.org/wiki/Dead_man%27s_switch)". An implementator of this concept went with the name of the "Last Will contract".

The owner creates two wallets. One of them is purely a cold-storage wallet and the only part we need is the address of this wallet. The second is our auto-renew wallet.

The beneficiary who inherits everything after the user dies (or at least, stops pressing the dead man's switch) also creates a wallet and provides the address to it to us.

We then create a locking script (see [code](https://github.com/KarolTrzeszczkowski/Electron-Cash-Last-Will-Plugin#the-last-will-contract)) and take the entire inheritance and deposits that in the smart contract.

The script we just created is a 'p2sh' script, which means we need to keep that script in a file, or we simply spend it instantly to store it on-chain in a UTXO.

there are 3 ways to spend the UTXO:

1. move all the money to a new UTXO that will be encumbered by exact same contract but "refresh" the timer to a new 6 months - needs "extender" key signature
2. just spend all the money to whatever outputs - needs a "owner" key signature
3. just spend all the money to whatever outputs - needs a "beneficiary" key signature + the OP_CLTV must validate, so this path will become available only if the "refresher" failed to move the money to a reset timer

We can observe that the full funds move every extension where we refresh the timer. This is safe because we defined that the owners key is in cold-storage and there is a separate key used to sign those extensions. That key can safely be stored in a wallet and that wallet can safely auto-extend the contract.

# Inheritance usecase -- Adding cashtokens

The 'beneficiary' of an inheritance in most societies is not just one person. For instance you'd have parents, siblings and spouse and children all getting a part.

We can replace the beneficiary key with a cash-token address token instead and in that way provide a lot more features and increase the security as well.

The way to look at this is that when the inheritance pays out, it doesn't just go to one person but instead it goes to another smart contract, which is based on cashtokens. This second smart contract then takes care of dividing the parts between relatives.

Additionally, updating the details of how the funds are to be divided is done completely separate from the original contract and that makes it safer because less things need to ever be changed in the timelock contract.

There are several approaches possible and cashtokens is quite new so the optimal solution with the best trade-offs is open to research. It definitely is possible to do this, hopefully we'll see a PR to this doc with one or two viable options in future.


